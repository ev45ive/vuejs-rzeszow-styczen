import { Module } from 'vuex'
import { Playlist } from '../src/models/Playlist';

interface PlaylistsState {
    items: Playlist[],
    query: string,
    selectedId: Playlist['id'] | null
}

export const PlaylistsStore: Module<PlaylistsState, any> = {
    state: {
        query: '123',
        items: [
            {
                id: 123,
                name: "Best of VueJS",
                favourite: true,
                color: "#ff00ff"
            },
            {
                id: 234,
                name: "Vue TOP20",
                favourite: true,
                color: "#ffff00"
            },
            {
                id: 345,
                name: "Vue Greatest Hits",
                favourite: true,
                color: "#00ffff"
            }],
        selectedId: 123
    },
    mutations: {
        addPlaylist(state, playlist: Playlist) {
            state.items.push(playlist)
        },
        select(state, id: Playlist['id']) {
            state.selectedId = id
        },
        update(state, payload: Playlist) {
            const index = state.items.findIndex(
                playlist => playlist.id === payload.id
            );
            state.items.splice(index, 1, payload);
        },
        setQuery(state, query: string) {
            state.query = query
        }
    },
    actions: {
        updatePlaylist({ commit }, payload: Playlist) {
            commit('update', payload)
        },
        selectPlaylist({ commit }, id) {
            commit('select', id)
        },
        createPlaylist({ commit, state }, payload: Partial<Playlist>) {
            const id = Date.now()
            const draft: Playlist = {
                id,
                name: '',
                favourite: false,
                color: '#000000',
                ...payload,
                // createdAt: Date.now()
            }
            if (draft.name) {
                commit('addPlaylist', draft)
            }
        },
        filter({ commit }, query) {
            commit('setQuery', query)
        }
    },
    getters: {
        query({query}){
            return query
        },
        playlists(state) {
            return state.items
        },
        selected({ items, selectedId }) {
            return items.find(
                p => p.id == selectedId
            )
        },
        favouriteCount(state) {
            return state.items.filter(
                p => p.favourite
            ).length
        },
        filteredPlaylists(state) {
            return state.items.filter(
                p => p.name.includes(state.query)
            )
        }
    }
}

// type Partial<T> = {
//     [key in keyof T]?: T[key]
// }
