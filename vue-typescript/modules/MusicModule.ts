import { Module } from 'vuex'
import { Playlist } from '../src/models/Playlist';
import { Album } from '../src/models/Album';
import { musicSearch } from '../src/services';

interface MusicState {
    query: string,
    results: Album[],
    isLoading: boolean,
    error?: string
}

export const MusicStore: Module<MusicState, any> = {
    namespaced: true,
    state: {
        query: '',
        results: [{
            id: "123",
            name: "Test Album",
            images: [
                {
                    url: "http://placecage.com/300/300"
                }
            ]
        }],
        isLoading: false
    },
    mutations: {
        startLoading(state) {
            state.isLoading = true;
            state.error = undefined
        },
        loadResults(state, payload) {
            state.results = payload
        },
        endLoading(state) {
            state.isLoading = false
        },
        setError(state, error) {
            state.error = error
        },
    },
    actions: {
        searchAlbums({ commit }, query: string) {
            commit('startLoading')
            musicSearch.search(query).then(albums => {
                commit('loadResults', albums)
                commit('endLoading')
            }).catch(error => {
                commit('setError', error)
                commit('endLoading')
            })
        }
    },
    getters: {
        results(state) {
            return state.results
        },
        query(state) {
            return state.query
        },
        error(state) {
            return state.error
        },
    }
}

// type Partial<T> = {
//     [key in keyof T]?: T[key]
// }
