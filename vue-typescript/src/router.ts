import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import MusicSearch from './views/MusicSearch.vue';
import Playlists from './views/Playlists.vue';

Vue.use(Router);

export default new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      // beforeEnter()
    },
    {
      path:'/search',
      name:'search',
      component: MusicSearch
    },
    {
      path:'/playlists',
      name:'playlists',
      component: Playlists
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
  ],
});
