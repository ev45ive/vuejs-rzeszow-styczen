
// Register the router hooks with their names
import Component from 'vue-class-component'
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate' // for vue-router 2.2+
])

import Vue from 'vue';
import './plugins/axios'
import App from './App.vue';
import 'bootstrap/dist/css/bootstrap.css';
import { AxiosStatic } from 'axios';


declare module 'vue/types/vue' {
  interface Vue {
    $axios: AxiosStatic;
  }
}



import router from './router';
import store from './store';
// import './registerServiceWorker';

//DEBUG:
(window as any).store = store;

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
