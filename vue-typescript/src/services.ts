import { MusicSearchService } from './services/MusicSearchService';
import { SecurityService } from './services/SecurityService';

export const security = new SecurityService()
security.getToken()

export const musicSearch = new MusicSearchService(security)