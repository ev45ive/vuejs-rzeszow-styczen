// npm i axios --save
import axios, { AxiosError } from 'axios';
import { AlbumsResponse } from '@/models/Album';
import { SecurityService } from './SecurityService';

export class MusicSearchService {

    constructor(
        protected security: SecurityService,
        protected url: string = 'https://api.spotify.com/v1/search'
    ) { }

    search(query: string = 'batman') {
        return axios.get<AlbumsResponse>(this.url, {
            params: {
                q: query,
                type: 'album'
            },
            headers:{
                'Authorization': `Bearer ${this.security.getToken()}`
            }
        })
        .then((resp) => {
            return resp.data.albums.items
        })
        .catch((err:AxiosError) => {
            if(err.response && err.response.data.error.status == 401){
                this.security.authorize()
            }
            return Promise.reject(err.response && err.response.data.error)
        })
    }


    /* async search(query: string = 'batman') {
        try {
            const resp = await axios.get<AlbumsResponse>(this.url, {
                params: {
                    q: query,
                    type: 'album'
                }
            });
            return resp.data.albums.items;
        }
        catch (err) {
            return console.log('Error:', err);
        }
    } */

}