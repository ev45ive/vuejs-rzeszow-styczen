export class SecurityService {

    constructor(
        private auth_url = 'https://accounts.spotify.com/authorize',
        private client_id = '1510708db0944a0c9e50d265537c2d89',
        private response_type = 'token',
        private redirect_uri = 'http://localhost:8080/'
    ) { }

    token = ''

    authorize() {
        const url = `${this.auth_url}?`
            + `client_id=${this.client_id}`
            + `&response_type=${this.response_type}`
            + `&redirect_uri=${this.redirect_uri}`

        sessionStorage.removeItem('token')

        // Redirect to login page:
        location.href = url
    }

    getToken() {
        this.token = JSON.parse(sessionStorage.getItem('token') || 'null')

        // Get token from url:
        if (!this.token) {
            const match = location.href.match(/#access_token=([^&]+)/)
            this.token = match ? match[1] : this.token;
            location.href = ''
            sessionStorage.setItem('token', JSON.stringify(this.token))
        }
        // If no token - redirect to login page again:
        if (!this.token) {
            return this.authorize()
        }
        return this.token;
    }
}