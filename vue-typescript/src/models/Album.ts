// https://developer.spotify.com/documentation/web-api/reference/object-model/#paging-object

export interface Album {
    id: string
    name: string
    images: AlbumImage[],
    artists?: Artist[],
}

export interface AlbumImage {
    url: string
    width?: number
    height?: number
}
export interface Artist {
    id: string
    name: string
}

export interface PagingObject<T>{
    items: T[]
}

export interface AlbumsResponse{
    albums: PagingObject<Album>
}