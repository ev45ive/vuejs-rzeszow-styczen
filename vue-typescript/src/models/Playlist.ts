// export class Playlist{} // typeof x == Playlist

interface NamedEntity {
    id: number;
    name: string;
}

export interface Playlist extends NamedEntity {
    favourite: boolean;
    color: string;
    /** 
        List of Tracks
    */
    tracks?: Track[];
    // tracks: Array<Track>;
}

export interface Track extends NamedEntity {

}