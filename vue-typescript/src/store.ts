import Vue from 'vue';
import Vuex from 'vuex';
import { PlaylistsStore } from '../modules/Playlists';
import { MusicStore } from '../modules/MusicModule';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    playlists: PlaylistsStore,
    music: MusicStore,

  },
  // Private API:
  state: {
    counter: 0,
  },
  mutations: {
    increment(state, payload = 1) {
      state.counter += payload
    },
    decrement(state, payload = 1) {
      state.counter -= payload
    },
  },
  // Public API:
  actions: {

  },
  getters: {

  }
});
